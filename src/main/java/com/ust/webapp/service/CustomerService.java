package com.ust.webapp.service;

import com.ust.webapp.entity.Customer;

public interface CustomerService {

	Customer addCustomer(Customer customer);
	 
	 Customer searchCustomerByNameAndMobile(String name,long mobile);
}
