package com.ust.webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.webapp.entity.Customer;
import com.ust.webapp.repository.CustomerRespository;



@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRespository repo;

	@Override
	public Customer addCustomer(Customer customer) {
		
		 return repo.save(customer);
	}

	@Override
	public Customer searchCustomerByNameAndMobile(String name, long mobile) {
		// TODO Auto-generated method stub
		return repo.findByNameAndMobile(name, mobile);
	}
}
