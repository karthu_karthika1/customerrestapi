package com.ust.webapp.dto;

import org.springframework.stereotype.Component;

@Component
public class OrderItemDto {

	private int id;
	private String itemName;
	private double price;
	private int customer_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	@Override
	public String toString() {
		return "OrderItemDto [id=" + id + ", ItemName=" + itemName + ", price=" + price + ", customer_id=" + customer_id
				+ "]";
	}
	
	
}
