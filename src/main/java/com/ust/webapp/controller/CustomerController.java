package com.ust.webapp.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ust.webapp.dto.OrderItemDto;
import com.ust.webapp.entity.Customer;
import com.ust.webapp.service.CustomerService;

@RestController
@RequestMapping(path="/api/v1")
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@PostMapping("/saveCustomer")
	public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer){
		return new ResponseEntity<Customer>(service.addCustomer(customer),HttpStatus.CREATED);
	
	}
	
	@GetMapping("/find/{name}/{mobile}")
	public ResponseEntity<Object> serachByNameandMobile(@PathVariable("name") String name,
			@PathVariable("mobile") long mobile) {
		Customer search = service.searchCustomerByNameAndMobile(name, mobile);
		if (search != null)
			return new ResponseEntity<>(search, HttpStatus.FOUND);
		else
			return new ResponseEntity<>("Data Not Found", HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/myOrder/{customerId}")
	public OrderItemDto[] GetCustomerOrderDetails(@PathVariable("customerId") int id) {
		String url="http://localhost:9002/api/v1/findOrder/"+id;
		
		OrderItemDto[] forObject=restTemplate.getForObject(url, OrderItemDto[].class);
		
		for(OrderItemDto orderItemDto :forObject) {
			System.out.println(orderItemDto);
		}
		return forObject;
	}
}
