package com.ust.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ust.webapp.entity.Customer;



	
	@Repository
	public interface CustomerRespository  extends JpaRepository<Customer,Integer>{

		// save() findAll() findById() deleteById() delete()
		
		// select * from customer where name="" and mobile=""
		
		// JPA - QueryMethods
		// private int id;	private String name;  private long mobile;
		
		// select * from customer where name="" and mobile=""
		Customer findByNameAndMobile(String name,long mobile);
		
		@Query("select cus from Customer cus where cus.mobile=?1")
		Customer findByMobile(String mobile);
	} 

